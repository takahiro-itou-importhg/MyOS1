//  -*-  coding: utf-8; mode: c++  -*-  //
/*************************************************************************
**                                                                      **
**                      --  My Operating System --                      **
**                                                                      **
**          Copyright (C), 2015-2015, Takahiro Itou                     **
**          All Rights Reserved.                                        **
**                                                                      **
*************************************************************************/

/**
**      割り込みハンドラ。
**
**      @file   initkernel/InterruptHandlers.c
**/

void  _int_21_handler(void)
{
}

void  _int_2c_handler(void)
{
}
